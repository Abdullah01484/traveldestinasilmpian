package com.example.TravelDestinasiImpian.repository;

import com.example.TravelDestinasiImpian.models.OrderDetail;
import com.example.TravelDestinasiImpian.models.OrderHeader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RepoOrderDetail extends JpaRepository<OrderDetail, Long> {

    @Query(value = "SELECT * FROM order_detail ORDER BY order_detail ASC", nativeQuery = true)
    List<OrderDetail> FindAllOrderDetailOrderByASC();

    @Query(value = "SELECT * FROM order_detail ORDER BY order_detail ASC", nativeQuery = true)
    Page<OrderDetail> FindAllOrderDetailOrderByASC(Pageable pageable);

    @Query("FROM OrderDetail WHERE OrderHeaderId = ?1")
    List<OrderDetail> FindByHeaderId(Long OrderHeaderId);
}
