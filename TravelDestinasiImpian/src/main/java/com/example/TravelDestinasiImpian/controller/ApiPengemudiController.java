package com.example.TravelDestinasiImpian.controller;

import com.example.TravelDestinasiImpian.models.Armada;
import com.example.TravelDestinasiImpian.models.Pengemudi;
import com.example.TravelDestinasiImpian.repository.RepoPengemudi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiPengemudiController {

    @Autowired
    private RepoPengemudi repoPengemudi;

    @GetMapping("/getAllPengemudi")
    public ResponseEntity<List<Pengemudi>> GetAllPengemudi()
    {
        try {
            List<Pengemudi> pengemudi = this.repoPengemudi.FindAllPengemudiOrderByASC();
            return new ResponseEntity<>(pengemudi, HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

    }

    @GetMapping("/getByIDPengemudi/{id}")
    public ResponseEntity<List<Pengemudi>>GetPengemudiById(@PathVariable("id") Long id)
    {
        try {
            Optional<Pengemudi> pengemudi = this.repoPengemudi.findById(id);

            if(pengemudi.isPresent()){
                ResponseEntity rest = new ResponseEntity<>(pengemudi, HttpStatus.OK);
                return rest;
            }else {
                return ResponseEntity.notFound().build();
            }
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/searchpengemudi/{keyword}")
    public ResponseEntity<List<Pengemudi>> SearchPengemudiName(@PathVariable("keyword") String keyword)
    {
        if (keyword != null)
        {
            List<Pengemudi> pengemudi = this.repoPengemudi.SearchPengemudi(keyword);
            return new ResponseEntity<>(pengemudi, HttpStatus.OK);
        } else {
            List<Pengemudi> pengemudi = this.repoPengemudi.findAll();
            return new ResponseEntity<>(pengemudi, HttpStatus.OK);
        }
    }

    @GetMapping("/pengemudimapped")
    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size)
    {
        try {
            List<Pengemudi> pengemudi = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);

            Page<Pengemudi> pageTuts;

            pageTuts = repoPengemudi.FindAllPengemudiOrderByASC(pagingSort);

            pengemudi = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("pengemudi", pengemudi);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/addpengemudi")
    public ResponseEntity<Object> SavePengemudi(@RequestBody Pengemudi pengemudi)
    {
        try {
            pengemudi.setCreatedBy("Syafiq");
            pengemudi.setCreatedOn(new Date());
            this.repoPengemudi.save(pengemudi);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/editpengemudi/{id}")
    public ResponseEntity<Object> UpdatePengemudi(@RequestBody Pengemudi pengemudi, @PathVariable("id") Long id)
    {
        Optional<Pengemudi> pengemudiData = this.repoPengemudi.findById(id);

        if (pengemudiData.isPresent()){
            pengemudi.setModifiedBy("Syafiq");
            pengemudi.setModifiedOn(new Date());
            pengemudi.setId(id);
            this.repoPengemudi.save(pengemudi);
            ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        }else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/deletepengemudi/{id}")
    public ResponseEntity<Object> deletePengemudi(@PathVariable("id") Long id)
    {
        this.repoPengemudi.deleteById(id);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }
}
