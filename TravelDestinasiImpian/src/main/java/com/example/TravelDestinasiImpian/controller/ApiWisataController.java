package com.example.TravelDestinasiImpian.controller;

import com.example.TravelDestinasiImpian.models.Armada;
import com.example.TravelDestinasiImpian.models.Wisata;
import com.example.TravelDestinasiImpian.repository.RepoWisata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiWisataController {

    @Autowired
    private RepoWisata repoWisata;

    @GetMapping("/getAllWisata")
    public ResponseEntity<List<Wisata>> GetAllWisata()
    {
        try {
            List<Wisata> wisata = this.repoWisata.FindAllWisataOrderByASC();
            return new ResponseEntity<>(wisata, HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

    }

    @GetMapping("/getByIDWisata/{id}")
    public ResponseEntity<List<Wisata>>GetWisataById(@PathVariable("id") Long id)
    {
        try {
            Optional<Wisata> wisata = this.repoWisata.findById(id);

            if(wisata.isPresent()){
                ResponseEntity rest = new ResponseEntity<>(wisata, HttpStatus.OK);
                return rest;
            }else {
                return ResponseEntity.notFound().build();
            }
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/searchwisata/{keyword}")
    public ResponseEntity<List<Wisata>> SearchArmadaName(@PathVariable("keyword") String keyword)
    {
        if (keyword != null)
        {
            List<Wisata> wisata = this.repoWisata.SearchWisata(keyword);
            return new ResponseEntity<>(wisata, HttpStatus.OK);
        } else {
            List<Wisata> wisata = this.repoWisata.findAll();
            return new ResponseEntity<>(wisata, HttpStatus.OK);
        }
    }

    @GetMapping("/wisatamapped")
    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size)
    {
        try {
            List<Wisata> wisata = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);

            Page<Wisata> pageTuts;

            pageTuts = repoWisata.FindAllWisataOrderByASC(pagingSort);

            wisata = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("wisata", wisata);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/addwisata")
    public ResponseEntity<Object> SaveWisata(@RequestBody Wisata wisata)
    {
        try {
            wisata.setCreatedBy("Syafiq");
            wisata.setCreatedOn(new Date());
            this.repoWisata.save(wisata);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/editwisata/{id}")
    public ResponseEntity<Object> Updatewisata(@RequestBody Wisata wisata, @PathVariable("id") Long id)
    {
        Optional<Wisata> wisataData = this.repoWisata.findById(id);

        if (wisataData.isPresent()){
            wisata.setModifiedBy("Syafiq");
            wisata.setModifiedOn(new Date());
            wisata.setId(id);
            this.repoWisata.save(wisata);
            ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        }else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/deletewisata/{id}")
    public ResponseEntity<Object> deleteWisata(@PathVariable("id") Long id)
    {
        this.repoWisata.deleteById(id);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }

    @GetMapping("/getwisatabykota/{kotaId}")
    public List<Wisata> getWisataBykota(@PathVariable("kotaId") Long kotaId) {
        return repoWisata.getWisataByKota(kotaId);
    }
}
