package com.example.TravelDestinasiImpian.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/orderdetail")
public class OrderDetailController {
    @RequestMapping("")
    public String orderdetail(){ return ("order/orderDetail"); }
}
