package com.example.TravelDestinasiImpian.controller;

import com.example.TravelDestinasiImpian.models.OrderHeader;
import com.example.TravelDestinasiImpian.repository.RepoOrderHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiOrderHeaderController {

    @Autowired
    private RepoOrderHeader repoOrderHeader;

    @GetMapping(value = "/getAllOrderHeader")
    public ResponseEntity<List<OrderHeader>> GetAllOrderHeader()
    {
        try {
            List<OrderHeader> orderHeader = this.repoOrderHeader.FindAllOrderHeaderOrderByASC();
            return new ResponseEntity<>(orderHeader, HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

    }

    @GetMapping(value = "/getByIDOrderHeader/{id}")
    public ResponseEntity<List<OrderHeader>> GetOrderHeaderById(@PathVariable("id") Long id)
    {
        try
        {
            Optional<OrderHeader> orderHeader = this.repoOrderHeader.findById(id);

            if (orderHeader.isPresent())
            {
                ResponseEntity rest = new ResponseEntity<>(orderHeader, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception exception)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping(value = "/getorderheadermaxid")
    public ResponseEntity<Long> GetOrderHeaderMaxId()
    {
        try
        {
            Long orderHeader = this.repoOrderHeader.GetMaxOrderHeader();
            System.out.print(orderHeader);
            return new ResponseEntity<>(orderHeader, HttpStatus.OK);
        }

        catch (Exception exception)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/searchorderheader/{keyword}")
    public ResponseEntity<List<OrderHeader>> SearchOrderHeaderName(@PathVariable("keyword") String keyword)
    {
        if (keyword != null)
        {
            List<OrderHeader> orderHeader = this.repoOrderHeader.SearchOrderHeader(keyword);
            return new ResponseEntity<>(orderHeader, HttpStatus.OK);
        } else {
            List<OrderHeader> orderHeader = this.repoOrderHeader.findAll();
            return new ResponseEntity<>(orderHeader, HttpStatus.OK);
        }
    }

    @GetMapping("/orderheadermapped")
    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size)
    {
        try {
            List<OrderHeader> orderHeader = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);

            Page<OrderHeader> pageTuts;

            pageTuts = repoOrderHeader.FindAllOrderHeaderOrderByASC(pagingSort);

            orderHeader = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("orderHeader", orderHeader);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        catch (Exception exception)
        {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/addorderheader")
    public ResponseEntity<Object> SaveOrderHeader(@RequestBody OrderHeader orderHeader)
    {
        String reference = "" + System.currentTimeMillis();
        orderHeader.setReference(reference);

        try
        {
            orderHeader.setCreatedBy("Syafiq");
            orderHeader.setCreatedOn(new Date());
            this.repoOrderHeader.save(orderHeader);
            return new ResponseEntity<>("Success", HttpStatus.CREATED);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "/editorderheader/{id}")
    public ResponseEntity<Object> UpdateOrderHeader(@RequestBody OrderHeader orderHeader, @PathVariable("id") Long id)
    {
        Optional<OrderHeader> orderHeaderData = this.repoOrderHeader.findById(id);

        if (orderHeaderData.isPresent())
        {
            orderHeader.setModifiedBy("Syafiq");
            orderHeader.setModifiedOn(new Date());
            orderHeader.setId(id);
            this.repoOrderHeader.save(orderHeader);
            ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.CREATED);
            return rest;
        } else {
            return ResponseEntity.notFound().build();
        }
    }

}
