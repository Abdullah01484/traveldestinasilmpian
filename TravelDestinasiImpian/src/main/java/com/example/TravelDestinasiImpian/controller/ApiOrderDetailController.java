package com.example.TravelDestinasiImpian.controller;

import com.example.TravelDestinasiImpian.models.OrderDetail;
import com.example.TravelDestinasiImpian.repository.RepoOrderDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiOrderDetailController {

    @Autowired
    private RepoOrderDetail repoOrderDetail;

    @GetMapping(value = "/getAllOrderDetail")
    public ResponseEntity<List<OrderDetail>> GetAllOrderDetail()
    {
        try {
            List<OrderDetail> orderDetail = this.repoOrderDetail.FindAllOrderDetailOrderByASC();
            return new ResponseEntity<>(orderDetail, HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

    }

    @GetMapping(value = "/getByIDOrderDetail/{id}")
    public ResponseEntity<List<OrderDetail>> GetOrderDetailById(@PathVariable("id") Long id)
    {
        try
        {
            List<OrderDetail> orderDetail = this.repoOrderDetail.FindByHeaderId(id);
            return new ResponseEntity<>(orderDetail, HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/addorderdetail")
    public ResponseEntity<Object> SaveOrderDetail(@RequestBody OrderDetail orderDetail)
    {
        try
        {
            orderDetail.setCreatedBy("Syafiq");
            orderDetail.setCreatedOn(new Date());
            this.repoOrderDetail.save(orderDetail);
            return new ResponseEntity<>("Success", HttpStatus.CREATED);
        }catch (Exception e){
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/editorderdetail/{id}")
    public ResponseEntity<Object> UpdateOrderDetail(@RequestBody OrderDetail orderDetail, @PathVariable("id") Long id)
    {
        Optional<OrderDetail> orderDetailData = this.repoOrderDetail.findById(id);

        if (orderDetailData.isPresent())
        {
            orderDetail.setModifiedBy("Syafiq");
            orderDetail.setModifiedOn(new Date());
            orderDetail.setId(id);
            this.repoOrderDetail.save(orderDetail);
            ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.CREATED);
            return rest;
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/deleteorderdetail/{id}")
    public ResponseEntity<Object> DeleteOrderDetail(@PathVariable("id") Long id)
    {
        this.repoOrderDetail.deleteById(id);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }

    @GetMapping("/orderdetailbyorder/{id}")
    public ResponseEntity<List<OrderDetail>> GetOrderById(@PathVariable("id") Long id)
    {
        try
        {
            List<OrderDetail> orderDetail = this.repoOrderDetail.FindByHeaderId(id);
            return new ResponseEntity<>(orderDetail, HttpStatus.OK);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }


}
