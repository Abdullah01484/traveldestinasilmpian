$(document).ready(function() {
	getWisataById();

})

function getWisataById() {
	var id = $("#delWisataId").val();
	$.ajax({
		url: "/api/deletewisata/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#initDel").text(data.id);
		}
	})
}

$("#delCancelBtn").click(function() {
	$(".modal").modal("hide")
})

$("#delDeleteBtn").click(function() {
	var id = $("#delWisataId").val();
	$.ajax({
		url : "/api/deletewisata/"+ id,
		type : "DELETE",
		contentType : "application/json",
		success: function(data){
				$(".modal").modal("hide")
				location.reload();
				GetAllWisata();

		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})