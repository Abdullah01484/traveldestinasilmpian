function getAllPengemudi(){
	$("#pengemudiTable").html(
		`<thead>
			<tr>
				<th>IdPengemudi</th>
				<th>NomorPegawai</th>
				<th>NamaPengemudi</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody id="pengemudiTBody"></tbody>
		`
	);

	$.ajax({
		url : "/api/getAllPengemudi",
		type : "GET",
		contentType : "application/json",
		success: function(data){
			for(i = 0; i<data.length; i++){
//			console.log(data[i].id)
				$("#pengemudiTBody").append(
					`
					<tr>
						<td>${data[i].id}</td>
						<td>${data[i].no_pegawai}</td>
						<td>${data[i].nm_pengemudi}</td>
                        <td>
                        	<button value="${data[i].id}" onClick="editPengemudi(this.value)" class="btn btn-warning">
                        	<i class="bi-pencil-square"></i>
                        	</button>
                        <td>
                        <td>
                        	<button value="${data[i].id}" onClick="deletePengemudi(this.value)" class="btn btn-danger">
                        	<i class="bi-trash"></i>
                        	</button>
                        </td>
					</tr>
					`
				)
			}
		}
	});
}

$("#addBtn").click(function(){
	$.ajax({
		url: "/pengemudi/addPengemudi",
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Create New Pengemudi");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
})

function editPengemudi(id){
	$.ajax({
		url: "/pengemudi/editpengemudi/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Edit Pengemudi");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function deletePengemudi(id){
	$.ajax({
		url: "/pengemudi/deletepengemudi/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Delete pengemudi");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}


function PengemudiList(currentPage, length) {
    $.ajax({
		url : '/api/pengemudimapped?page=' + currentPage + '&size=' + length,
		type : 'GET',
		contentType : 'application/json',
		success : function(data) {

			let table = '<select class="custom-select mt-3" id="size" onchange="PengemudiList(0,this.value)">'
//			table += '<option value="3" selected>..</option>'
			table += '<option value="5">5</option>'
			table += '<option value="10">10</option>'
			table += '<option value="15">15</option>'
			table += '</select>'
			table += "<table class='table table-bordered mt-3'>";
			table += "<tr> <th width='10%' class='text-center'>ID Pengemudi</th> <th>Nomor Pegawai</th> <th>Nama Pengemudi</th> <th>Action</th> </tr>"
			for (let i = 0; i < data.pengemudi.length; i++) {
				table += "<tr>";
				table += "<td class='text-center'>" + ((i + 1) + (data.currentPage * length)) + "</td>";
				table += "<td>" + data.pengemudi[i].no_pegawai + "</td>";
				table += "<td>" + data.pengemudi[i].nm_pengemudi + "</td>";
				table += "<td><button class='btn btn-primary btn-sm' value='" + data.pengemudi[i].id + "' onclick=editPengemudi(this.value)>Edit</button></td>";
				table += "<td><button class='btn btn-danger btn-sm' value='" + data.pengemudi[i].id + "' onclick=deletePengemudi(this.value)>Delete</button></td>";
				// table += "<td><input type='checkbox' onclick='SelectedItem()' class='mychecked' id='listdelete' value='"+data.category[i].id+"'></td>"
				table += "</tr>";
			}
				table += "</table>";
				table += "<br>"
				table += '<nav aria-label="Page navigation">';
				table += '<ul class="pagination">'
				table += '<li class="page-item"><a class="page-link" onclick="PengemudiList(' + (data.currentPage - 1) + ',' + length + ')">Previous</a></li>'
				let index = 1;
				for (let i = 0; i < data.totalPages; i++) {
					table += '<li class="page-item"><a id="pageslink" class="page-link" onclick="PengemudiList(' + i + ',' + length + ')">' + index + '</a></li>'
					index++;
				}
			table += '<li class="page-item"><a class="page-link" onclick="PengemudiList(' + (data.currentPage + 1) + ',' + length + ')">Next</a></li>'
			table += '</ul>'
			table += '</nav>';
			$('#pengemudiList').html(table);
		}

	});
}


//function SearchPengemudi(request) {
////console.log(request)
//
//    if (request.length > 0)
//    {
//        $.ajax({
//        url: '/api/searchpengemudi/' + request,
//        type: 'GET',
//        contentType: 'application/json',
//        success: function (result) {
//                //console.log(result)
//                if (result.length > 0)
//                {
//                    for (let i = 0; i < result.length; i++) {
//                    $("#pengemudiTBody").html(
//                        `
//                    	<tr>
//                    		<td>${result[i].id}</td>
//                    		<td>${result[i].no_pegawai}</td>
//                    		<td>${result[i].nm_pengemudi}</td>
//                            <td>
//                                <button value="${result[i].id}" onClick="editPengemudi(this.value)" class="btn btn-warning">
//                                    <i class="bi-pencil-square"></i>
//                                </button>
//                                <button value="${result[i].id}" onClick="deletePengemudi(this.value)" class="btn btn-danger">
//                                    <i class="bi-trash"></i>
//                                </button>
//                            </td>
//                    	</tr>
//                    	`)
//                    }
//                }
//                else {
//                    $("#pengemudiTBody").html(
//                    `<tr>
//                        <td colspan='3' class='text-center'>No data</td>
//                    </tr>`
//                    );
//                }
//        },
//            error: function (error){
//                console.error('Error occurred', error);
//            }
//        });
//    }
//    else {
//	    getAllPengemudi();
//    }
//}

function SearchPengemudi(request) {
//console.log(request)

    if (request.length > 0)
	{
	    $.ajax({
			url: '/api/searchpengemudi/' + request,
			type: 'GET',
			contentType: 'application/json',
			success: function (result) {
			    //console.log(result)
			    let table = "<table class='table table-bordered mt-3'>";
				table += "<tr> <th width='10%' class='text-center'>ID Pengemudi</th> <th>Nomor Pegemudi</th> <th>Nama Pengemudi</th> <th>Action</th> </tr>"
			    if (result.length > 0)
			    {
				    for (let i = 0; i < result.length; i++) {
					    table += "<tr>";
					    table += "<td class='text-center'>" + (i+1) + "</td>";
                        table += "<td>" + result[i].no_pegawai + "</td>";
                        table += "<td>" + result[i].nm_pengemudi + "</td>";
                        table += "<td>" + result[i].Action + "</td>";
                        table += "<td><button class='btn btn-primary btn-sm' value='" + result[i].id + "' onclick=editPengemudi(this.value)>Edit</button></td>";
                        table += "<td><button class='btn btn-danger btn-sm' value='" +  result[i].id + "' onclick=deletePengemudi(this.value)>Delete</button></td>";

						table += "</tr>";
					}
				} else {
				    table += "<tr>";
				    table += "<td colspan='4' class='text-center'>No data</td>";
				    table += "</tr>";
		        }
				table += "</table>";
				$('#pengemudiList').html(table);
			}
		});
	} else {
        PengemudiList(0,5);
	}
}

$(document).ready(function(){
//	getAllPengemudi();
PengemudiList(0,5);
})