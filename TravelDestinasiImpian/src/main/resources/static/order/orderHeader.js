
        $("#addBtn").click(function(){
        	$.ajax({
        		url: "/orderdetail/",
        		type: "GET",
        		contentType: "html",
        		success: function(data){
        		     var reference = '{"reference":"","totalHarga":"0"}';
                         $.ajax({
                             url:'/api/addorderheader',
                             type:'post',
                             contentType:'application/json',
                             data:reference,
                             async: false,
                             success:function(data) {
                                   $.ajax({
                                       url:'/api/getorderheadermaxid',
                                       type:'get',
                                       contentType:'application/json',
                                       success:function(data) {
                                             console.log("maxID: ", data)
                                             $('#orderHeaderId').val(data);
                                            }
                                        })
                                    }
                                })

        			$(".modal-title").text("Create Order");
        			$(".modal-body").html(data);
        			$("#myModal").modal("show");
        		}
        	});
        })

//        function OpenList(currentPage, length) {
//            $.ajax({
//                url: '/api/orderheadermapped?page=' + currentPage + '&size=' + length,
//                type: 'GET',
//                contentType: 'application/json',
//                success: function (order) {
//                    console.log(order)
//                    let table = '<select class="custom-select mt-3" id="size" onchange="OpenList(0,this.value)">'
//                    table += '<option value="3" selected>..</option>'
//                    table += '<option value="5">5</option>'
//                    table += '<option value="10">10</option>'
//                    table += '<option value="15">15</option>'
//                    table += '</select>'
//                    table += "<table class='table table-bordered mt-3'>";
//                    table += "<tr> <th>#</th> <th>Reference</th> <th>Amount</th> <th>Action</th> </tr>"
//
//                    if (order.orderHeader.length > 0)
//                    {
//                        for (let i = 0; i < order.orderHeader.length; i++) {
//                            table += "<tr>"
//                            table += "<td>"+ (i+1) +"</td>"
//                            table += "<td>"+ order.orderHeader[i].reference +"</td>"
//                            table += "<td>"+ order.orderHeader[i].totalHarga +"</td>"
//                            table += "<td> <button class='btn btn-sm btn-primary' onclick='GetDetail("+ order.orderHeader[i].id +")'>Detail</button> </td>"
//                            table += "</tr>"
//                        }
//                    } else {
//                        table += "<tr> <td colspan='3' class='text-center'>No Data</td> </tr>"
//                    }
//
//                    table += "</table>";
//                    table += "<br>"
//                    table += '<nav aria-label="Page navigation">';
//                    table += '<ul class="pagination">'
//                    table += '<li class="page-item"><a class="page-link" onclick="OpenList(' + (order.currentPage - 1) + ',' + length + ')">Previous</a></li>'
//                    let index = 1;
//                    for (let i = 0; i < order.totalPages; i++) {
//                        table += '<li class="page-item"><a id="pageslink" class="page-link" onclick="OpenList(' + i + ',' + length + ')">' + index + '</a></li>'
//                        index++;
//                    }
//                    table += '<li class="page-item"><a class="page-link" onclick="OpenList(' + (order.currentPage + 1) + ',' + length + ')">Next</a></li>'
//                    table += '</ul>'
//                    table += '</nav>';
//                    $('#dataList').html(table)
//                }
//            });
//        }



        function GetDetail(orderHeaderId)
        {
            console.log(orderHeaderId)
            $.ajax({
                url: '/api/orderdetailbyorder/' + orderHeaderId,
                type: 'GET',
                contentType: 'application/json',
                success: function (detail)
                {
                    console.log("detail: ", detail)
                    let table= '<table class="table table-bordered">';
                    table += "<tr> <th>Nama Pemesan</th> <th>Kota</th> <th>Wisata</th> <th>Harga</th> </tr>"
                    if (detail.length > 0)
                    {
                        for (let i = 0; i < detail.length; i++) {
                            table += "<tr>"
                            table += "<td>" + detail[i].namaPemesan + "</td>"
                            table += "<td>" + detail[i].kota.namaKota + "</td>"
                            table += "<td>" + detail[i].wisata.namaWisata + "</td>"
                            table += "<td>" + detail[i].wisata.harga + "</td>"
                            table += "</tr>"
                        }
                    } else {
                        table += "<tr>"
                        table += "<td colspan='5' class='text-center'>No Data</td>"
                        table += "</tr>"
                    }
                    table += "</table>";
                    $('.modal-title').html("Order Amount");
                    $('.modal-body').html(table);
                    $('#myModal').modal('show')
                }
            });
        }

//        function SearchOrderHeader(request) {
//            //console.log(request)
//
//            if (request.length > 0)
//            {
//                $.ajax({
//                    url: '/api/searchorderheader/' + request,
//                    type: 'GET',
//                    contentType: 'application/json',
//                    success: function (result) {
//                        //console.log(result)
//                        let table = "<table class='table table-bordered mt-3'>"
//                        table += "<tr> <th>#</th> <th>Reference</th> <th>Amount</th> <th>Action</th> </tr>"
//                        if (result.length > 0)
//                        {
//                            for (let i = 0; i < result.length; i++) {
//                                table += "<tr>"
//                                table += "<td>"+ (i+1) +"</td>"
//                                table += "<td>"+ result[i].reference +"</td>"
//                                table += "<td>"+ result[i].totalHarga +"</td>"
//                                table += "<td> <button class='btn btn-sm btn-primary' onclick='GetDetail("+ result[i].id +")'>Detail</button> </td>"
//                                table += "</tr>"
//                            }
//                        } else {
//                            table += "<tr>";
//                            table += "<td colspan='5' class='text-center'>No data</td>";
//                            table += "</tr>";
//                        }
//
//                        table += "</table>";
//                        $('#dataList').html(table)
//                    }
//                });
//            } else {
//                OpenList(0,5);
//            }
//        }


    $(document).ready(function(){
	    OpenList(0,5);
    })