        function DataList() {
            $.ajax({
                url:'/api/orderdetailbyorder/' + $('#orderHeaderId').val(),
                type:'get',
                contentType:'application/json',
                success:function(data) {
                    console.log("dataList: ",data)
                    let table = "<table class='table table-bordered'>"
                    table += "<tr>"
                    table += "<td>Kota</td>"
                    table += "<td>Wisata</td>"
                    table += "<td>Harga</td>"
                    table += "<td>Action</td>"
                    table += "</tr>"
                    let totalHarga = 0;
                    for(i=0; i<data.length; i++) {
                        var ref = data[i].orderHeader.reference;
                        table += "<tr>"
                        table += "<td>"+data[i].kota.nm_kota+"</td>"
                        table += "<td>"+data[i].wisata.nm_wisata+"</td>"
                        table += "<td>"+data[i].wisata.harga+"</td>"
                        table += "<td><button value='" + data[i].id + "' class='btn btn-sm btn-danger' onclick='MinOrder(this.value)'>-</button></td>"
                        table += "</tr>"
                        totalHarga += data[i].wisata.harga;
                    }
                    table += "<tr>"
                    table += "<td>Total Harga</td>"
                    table += "<td><input type='text' id='totalHarga' class='form-control' value='"+totalHarga+"' readonly></td>"
                    table += "</tr>"
                    table += "</table>"
                    table += "<input type='text' class='form-control' id='ref' value='"+ref+"'>"
                    table += "<p><button class='btn btn-sm btn-primary' onclick='DoneProcess()'>Done</button></p>"
                    $('#dataList').html(table)
                }
            })
        }

        function getAllKota() {
        	$.ajax({
        		url: "/api/getAllKota",
        		type: "GET",
        		contentType: "application/json",
        		success: function(data) {
        			for (i = 0; i < data.length; i++) {
        				$("#kotaSelect").append(
        					`<option value="${data[i].id}">${data[i].nm_kota}</option>`
        				)

        			}
        		}
        	});
        }

        function changeWisata(id) {
        	$(".varOption").remove();
        	$.ajax({
        		url: "/api/getwisatabykota/" + id,
        		type: "GET",
        		contentType: "application/json",
        		success: function(data) {
        			if (data[0] != null) {
        				for (i = 0; i < data.length; i++) {
        					$("#wisataSelect").append(
        						`<option class="varOption" value="${data[i].id}">${data[i].nm_wisata}</option>`
        					)
        				}
        			} else {
        				$("#varSelect").append(
        					`<option class="varOption" value="1" disabled="true">Empty</option>`
        				)
        			}
        		},
        	});
        }

        function GetPrice(id) {
            $.ajax({
                url:'/api/getByIDWisata/'+id,
                type:'get',
                contentType:'application/json',
                success:function(data) {
                    console.log(data)
                    $('#harga').val(data.harga);
                }
            })
        }

        function MinOrder(request) {
            console.log(request)
            $.ajax({
                url:'/api/deleteorderdetail/' + request,
                type:'delete',
                contentType:'application/json',
                success:function(data) {
                    DataList();
                }
            })
        }

        function AddOrder() {
            var detailData = '{';
            detailData += '"orderHeaderId":"'+$('#orderHeaderId').val()+'",';
            detailData += '"kotaId":"'+$('#kotaSelect').val()+'",';
            detailData += '"wisataId":"'+$('#kotaSelect').val()+'",';
            detailData += '"harga":"'+$('#harga').val()+'"';
            detailData += '}';
            $.ajax({
                url:'/api/addorderdetail/',
                type:'post',
                contentType:'application/json',
                data:detailData,
                success:function(data) {
                    DataList();
                }
            })

        }

        function DoneProcess() {
            var dataHeader = '{ "namaPemesan":"'+$('#namaPemesan').val()+'","reference":"'+$('#ref').val()+'","totalHarga":"'+$('#totalHarga').val()+'" }'
            $.ajax({
                url:'/api/editorderheader/'+$('#orderHeaderId').val(),
                type:'put',
                contentType:'application/json',
                data:dataHeader,
                success:function(data) {
                    console.log("orderHeader: ", data);
                    $("#myModal").modal("hide")
                    location.reload();

                }
            })
        }

        $(document).ready(function() {
        	getAllKota();
        })